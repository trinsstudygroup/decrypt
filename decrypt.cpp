#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

int main() {

    int offset, size, sizedot, count=0, dummy=0, total;
    string enmsg, msg="", off, dot="";

    ifstream indata;
    indata.open("secret.dat");
    if (!indata)
    {
        cout << "Error!!! The file was not found" << endl;
        return 1;
    }

    getline(indata, enmsg);
    size = enmsg.length();

    //read the offset offset and place it as an int
    off.clear();
    for (int i = 0; i < 2 ; i++) {
        if (isdigit(enmsg[i])){
            off += enmsg[i];
            count++;
        }
    }
    //convert offset string to int
    stringstream convert(off);
    (convert >> offset);

    // once the int is read, read a char one at a time until an &

    for(int i = count; i < size; i++) {

// Start for the letters

        if (enmsg[i] >= 'a' && enmsg[i] <= 'z') {
            if ((enmsg[i] - offset) < 'a') {
                msg += ((enmsg[i] - offset) + 26);
            }
            else {
                msg += (enmsg[i] - offset);
            }
        }

// Start for the numbers

        else if (enmsg[i] >= '0' && enmsg[i] <= '9') {
            if ((enmsg[i] - offset) < '0') {
                if ((enmsg[i] - offset) + 10 < '0') {
                    msg += ((enmsg[i] - offset) + 20);
                }
                else {
                    msg += ((enmsg[i] - offset) + 10);
                }
            }
        }
        if (enmsg[i] == '&'){
            break;
        }
        dummy++;
    }
    total = (dummy + 1) + count; //total of array up to the '&'

    for (int i = total; i < size; i++) {
            dot += enmsg[i];
    }

    sizedot = dot.length();

    if (sizedot !=0) {
        off.clear();
        int i = 0;
        for (; i < sizedot + 1; i++) {
            if (isdigit(dot[i])) {
                off += dot[i];
            }
            else {
                stringstream convert(off);
                (convert >> offset);
                msg[offset - 1] = ' ';
                off.clear();
            }
        }
    }

    cout << msg << endl;

    remove("secret.dat");
    return 0;
}